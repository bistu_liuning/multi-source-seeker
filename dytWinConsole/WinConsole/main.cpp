#include <QCoreApplication>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/highgui/highgui_c.h>
#include <stdio.h>
#include <stdint.h>
#include <iostream>
using namespace cv;
using namespace std;

Mat image;   //视频流
Mat imageCopy; //绘制矩形框时用来拷贝原图的图像
Mat rectImage;  //子图像
bool leftButtonDownFlag=false; //左键单击后视频暂停播放的标志位
Point originalPoint; //矩形框起点
Point processPoint; //矩形框终点

int resultRows;  //模板匹配result的行
int resultcols;  //模板匹配result的列
Mat ImageResult;  //模板匹配result
double minValue;   //模板匹配result最小值
double maxValude;   //模板匹配result最大值
Point minPoint;   //模板匹配result最小值位置
Point maxPoint;    //模板匹配result最大值位置
int frameCount=0; //帧数统计

VideoCapture video;

void onMouse(int event,int x,int y,int flags ,void* ustc); //鼠标回调函数


//*******************************************************************//
//鼠标回调函数
void onMouse(int event,int x,int y,int flags,void *ustc)
{
    if(event==CV_EVENT_LBUTTONDOWN)
    {
        leftButtonDownFlag=true; //标志位
        originalPoint=Point(x,y);  //设置左键按下点的矩形起点
        processPoint=originalPoint;
    }
    if(event==CV_EVENT_MOUSEMOVE&&leftButtonDownFlag)
    {
        imageCopy=image.clone();
        processPoint=Point(x,y);
        if(originalPoint!=processPoint)
        {
            //在复制的图像上绘制矩形
            rectangle(imageCopy,originalPoint,processPoint,Scalar(0,0,255),2);
        }
        imshow("Man",imageCopy);
    }
    if(event==CV_EVENT_LBUTTONUP)
    {
        leftButtonDownFlag=false;
        Mat subImage=image(Rect(originalPoint,processPoint)); //子图像
        rectImage=subImage.clone();
        resultRows=image.rows-rectImage.rows+1;
        resultcols=image.cols-rectImage.rows+1;
        imshow("Sub Image",rectImage);
    }
}
int main(int argc,char*argv[])
{
    QCoreApplication a(argc, argv);
    double fps=20; //获取视频帧率
    video.open(0);
    double pauseTime=1000/fps; //两幅画面中间间隔
    namedWindow("Man",0);
    setMouseCallback("Man",onMouse);
    while(true)
    {
        if(!leftButtonDownFlag) //鼠标左键按下绘制矩形时，视频暂停播放
        {
            video>>image;
            frameCount++;   //帧数
        }
        if(!image.data||waitKey(pauseTime+30)==27)  //图像为空或Esc键按下退出播放
        {
            break;
        }
        if(rectImage.data)
        {
            ImageResult=Mat::zeros(resultRows,resultcols,CV_32FC1);
            matchTemplate(image,rectImage,ImageResult,CV_TM_SQDIFF);  //模板匹配
            minMaxLoc(ImageResult,&minValue,&maxValude,&minPoint,&maxPoint,Mat());  //最小值最大值获取
            rectangle(image,minPoint,Point(minPoint.x+rectImage.cols,minPoint.y+rectImage.rows),Scalar(0,0,255),2);
            //更新当前模板匹配的模板
            Mat resultImage=image(Rect(minPoint,Point(minPoint.x+rectImage.cols,minPoint.y+rectImage.rows)));
            rectImage=resultImage.clone();
            //当前帧数输出到视频流
            stringstream ss;
            ss<<frameCount;
            string h="Current frame is: ";
            string fff=h+ss.str();
            putText(image,fff,Point(50,60),CV_FONT_HERSHEY_COMPLEX_SMALL,2,Scalar(0,0,255),2);
        }
        imshow("Man",image);
    }
    return a.exec();
}
